package kz.astana.moviecatalog

import android.app.Application
import kz.astana.moviecatalog.common.KoinModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber
import timber.log.Timber.DebugTree

class App : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@App)
            modules(KoinModule.createModule())
        }

        if (BuildConfig.DEBUG) Timber.plant(DebugTree())
    }
}