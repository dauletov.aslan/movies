package kz.astana.moviecatalog

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kz.astana.moviecatalog.catalog.CatalogFragment
import kz.astana.moviecatalog.common.replaceFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        replaceFragment(CatalogFragment.create())
    }
}