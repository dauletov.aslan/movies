package kz.astana.moviecatalog.catalog

import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_catalog.view.movieDate
import kotlinx.android.synthetic.main.item_catalog.view.movieDescription
import kotlinx.android.synthetic.main.item_catalog.view.movieFavourite
import kotlinx.android.synthetic.main.item_catalog.view.movieName
import kotlinx.android.synthetic.main.item_catalog.view.moviePoster
import kz.astana.moviecatalog.R
import kz.astana.moviecatalog.common.BaseAdapter
import kz.astana.moviecatalog.common.BaseViewHolder
import kz.astana.moviecatalog.common.loadImage

class CatalogAdapter(private val itemClickListener: (item: Movie) -> Unit) : BaseAdapter<Movie>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Movie> =
        CatalogViewHolder(parent, R.layout.item_catalog, itemClickListener)

    private class CatalogViewHolder(parent: ViewGroup, layoutId: Int, listener: ((Movie) -> Unit)? = null) :
        BaseViewHolder<Movie>(parent, layoutId, listener) {

        val poster: ImageView = itemView.moviePoster
        val name: TextView = itemView.movieName
        val description: TextView = itemView.movieDescription
        val date: TextView = itemView.movieDate
        val favourite: ImageView = itemView.movieFavourite

        override fun onBind(item: Movie) {
            super.onBind(item)

            Glide.with(itemView.context)
                .loadImage(item.posterPath)
                .into(poster)

            name.text = item.title
            description.text = item.overview
            date.text = item.releaseDate

            favourite.setOnClickListener {
                if (favourite.isActivated) {
                    favourite.setImageResource(R.drawable.ic_favorite_border)
                    favourite.isActivated = false
                } else {
                    favourite.setImageResource(R.drawable.ic_favorite)
                    favourite.isActivated = true
                }
            }
        }

        override fun onViewRecycled() {
            super.onViewRecycled()
            Glide.with(itemView.context).clear(poster)
        }
    }
}