package kz.astana.moviecatalog.catalog

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface CatalogApi {
    @GET("/3/discover/movie")
    fun requestMovieList(@QueryMap data: HashMap<String, Any>): Single<Movies>

    @GET("/3/search/movie")
    fun searchMovie(@QueryMap data: HashMap<String, Any>): Single<Movies>
}