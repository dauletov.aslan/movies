package kz.astana.moviecatalog.catalog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import kotlinx.android.synthetic.main.fragment_catalog.catalog
import kotlinx.android.synthetic.main.fragment_catalog.refreshButton
import kotlinx.android.synthetic.main.fragment_catalog.search
import kotlinx.android.synthetic.main.fragment_catalog.stateContainer
import kotlinx.android.synthetic.main.fragment_catalog.stateImage
import kotlinx.android.synthetic.main.fragment_catalog.stateText
import kotlinx.android.synthetic.main.fragment_catalog.swipeRefreshLayout
import kz.astana.moviecatalog.R
import org.koin.androidx.viewmodel.ext.android.viewModel

private const val CATALOG_POSITION = "CATALOG_POSITION"

class CatalogFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener {

    companion object {
        fun create() = CatalogFragment()
    }

    private val catalogViewModel: CatalogViewModel by viewModel()
    private lateinit var catalogAdapter: CatalogAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.fragment_catalog, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        swipeRefreshLayout.setOnRefreshListener(this)
        initRecycler()

        catalogViewModel.loadingState.observe(viewLifecycleOwner, Observer { swipeRefreshLayout.isRefreshing = it })
        catalogViewModel.errorState.observe(viewLifecycleOwner, Observer { error ->
            if (error) {
                catalog.isVisible = false
                stateContainer.isVisible = true
                stateImage.setImageResource(R.drawable.ic_error)
                stateText.setText(R.string.error_state)
                refreshButton.isVisible = true
            } else {
                catalog.isVisible = true
                stateContainer.isVisible = false
                refreshButton.isVisible = false
            }
        })
        catalogViewModel.moviesLiveData.observe(viewLifecycleOwner, Observer { movies ->
            if (movies.isEmpty()) {
                catalog.isVisible = false
                stateContainer.isVisible = true
                stateImage.setImageResource(R.drawable.ic_search)
                stateText.text = getString(R.string.empty_state, search.text)
            } else {
                catalog.isVisible = true
                stateContainer.isVisible = false
                catalogAdapter.setItems(movies)
            }
        })

        search.doAfterTextChanged {
            catalogViewModel.onSearch(it.toString())
        }

        refreshButton.setOnClickListener {
            catalogViewModel.onRefreshListNeed()
        }
    }

    override fun onRefresh() {
        catalogViewModel.onRefreshListNeed()
    }

    private fun initRecycler() {
        if (!::catalogAdapter.isInitialized) {
            catalogAdapter = CatalogAdapter { item ->
                Toast.makeText(requireContext(), item.title, Toast.LENGTH_SHORT).show()
            }
        }

        catalog.layoutManager = LinearLayoutManager(requireContext())
        catalog.adapter = catalogAdapter
        catalog.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                val directionDown = 1
                val directionUp = -1
                if (
                    !catalogAdapter.isEmpty() &&
                    !catalog.canScrollVertically(directionDown) &&
                    catalog.canScrollVertically(directionUp)
                ) {
                    catalogViewModel.onScrolledToEnd()
                }
            }
        })
    }
}