package kz.astana.moviecatalog.catalog

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

private const val API_KEY = "6ccd72a2a8fc239b13f209408fc31c33"

class CatalogViewModel(private val catalogApi: CatalogApi) : ViewModel() {

    val moviesLiveData = MutableLiveData<List<Movie>>()
    val loadingState = MutableLiveData<Boolean>()
    val errorState = MutableLiveData<Boolean>()
    private var disposable: Disposable? = null
    private var currentPage = 1
    private var isSearchMode = false

    init {
        requestMoviesList(currentPage)
    }

    fun onRefreshListNeed() {
        disposable?.dispose()
        moviesLiveData.value = emptyList()
        currentPage = 1
        requestMoviesList(1)
    }

    fun onScrolledToEnd() {
        if (!isSearchMode) {
            disposable?.dispose()
            requestMoviesList(++currentPage)
        }
    }

    fun onSearch(text: String) {
        if (text.isEmpty()) {
            isSearchMode = false
            onRefreshListNeed()
        } else {
            isSearchMode = true
            loadingState.value = true
            disposable?.dispose()
            disposable = catalogApi
                .searchMovie(
                    hashMapOf(
                        "api_key" to API_KEY,
                        "language" to "ru-RU",
                        "query" to text
                    )
                )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        moviesLiveData.value = it.movies
                        loadingState.value = false
                        errorState.value = false
                    },
                    {
                        loadingState.value = false
                        errorState.value = true
                        Timber.e(it)
                    }
                )
        }
    }

    override fun onCleared() {
        super.onCleared()
        disposable?.dispose()
    }

    private fun requestMoviesList(page: Int) {
        loadingState.value = true
        disposable = catalogApi
            .requestMovieList(
                hashMapOf(
                    "api_key" to API_KEY,
                    "language" to "ru-RU",
                    "page" to page
                )
            )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    moviesLiveData.value = addMoviesList(it.movies)
                    loadingState.value = false
                },
                {
                    loadingState.value = false
                    Timber.e(it)
                }
            )
    }

    private fun addMoviesList(movies: List<Movie>): List<Movie> {
        val moviesList = moviesLiveData.value?.toMutableList() ?: mutableListOf()
        moviesList.addAll(movies)
        return moviesList
    }
}