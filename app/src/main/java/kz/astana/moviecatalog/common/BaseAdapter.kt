package kz.astana.moviecatalog.common

import androidx.recyclerview.widget.RecyclerView

abstract class BaseAdapter<T> : RecyclerView.Adapter<BaseViewHolder<T>>() {

    protected val data = mutableListOf<T>()

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: BaseViewHolder<T>, position: Int) = holder.onBind(data[position])

    override fun onViewRecycled(holder: BaseViewHolder<T>) {
        super.onViewRecycled(holder)
        holder.onViewRecycled()
    }

    open fun getItem(position: Int): T = data[position]

    fun getItemRange(startPosition: Int, endPosition: Int) = data.subList(startPosition, endPosition)

    fun isEmpty() = data.size == 0

    fun clear() {
        data.clear()
        notifyDataSetChanged()
    }

    open fun setItems(items: List<T>) {
        data.clear()
        data.addAll(items)
        notifyDataSetChanged()
    }

    fun removeItem(element: T) {
        val position = data.indexOf(element)
        if (position != -1) {
            data.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun removeItem(predicate: (T) -> Boolean) {
        val position = data.indexOfFirst(predicate)
        if (position != -1) {
            data.removeAt(position)
            notifyItemRemoved(position)
        }
    }
}