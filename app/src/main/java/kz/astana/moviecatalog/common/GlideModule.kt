package kz.astana.moviecatalog.common

import android.content.Context
import android.graphics.drawable.Drawable
import com.bumptech.glide.GlideBuilder
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.RequestManager
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.load.Option
import com.bumptech.glide.module.AppGlideModule
import com.bumptech.glide.request.RequestOptions

const val GLIDE_BASE_URL = "base_url"

@GlideModule
class GlideModule : AppGlideModule() {
    override fun applyOptions(context: Context, builder: GlideBuilder) {
        val options = RequestOptions()
        val baseUrl = "https://image.tmdb.org/t/p/w342"
        options.set(Option.memory(GLIDE_BASE_URL), baseUrl)
        builder.setDefaultRequestOptions(options)
    }
}

fun <T> RequestBuilder<T>.loadImage(filePath: String?): RequestBuilder<T> {
    val baseUrl = options.get(Option.memory(GLIDE_BASE_URL)) ?: ""
    return load("$baseUrl${filePath.orEmpty()}")
}

fun RequestManager.loadImage(filePath: String?): RequestBuilder<Drawable> =
    asDrawable().loadImage(filePath)