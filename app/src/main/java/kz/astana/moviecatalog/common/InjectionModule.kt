package kz.astana.moviecatalog.common

import org.koin.core.module.Module

interface InjectionModule {
    fun createModule(): Module
}