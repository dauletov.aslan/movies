package kz.astana.moviecatalog.common

import android.content.Context
import kz.astana.moviecatalog.catalog.CatalogApi
import kz.astana.moviecatalog.catalog.CatalogViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object KoinModule : InjectionModule {
    override fun createModule() = module {
        single {
            val context = get<Context>()
            OkHttpFactory.create(context).build()
        }

        single {
            Retrofit.Builder()
                .baseUrl("https://api.themoviedb.org")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(get())
                .build()
        }

        single { get<Retrofit>().create(CatalogApi::class.java) }
        viewModel { CatalogViewModel(get()) }
    }
}