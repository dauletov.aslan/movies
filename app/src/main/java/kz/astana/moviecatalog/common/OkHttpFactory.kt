package kz.astana.moviecatalog.common

import android.content.Context
import okhttp3.Cache
import okhttp3.CertificatePinner
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit

private const val DEFAULT_CONNECT_TIMEOUT_SECONDS = 30L
private const val DEFAULT_READ_TIMEOUT_SECONDS = 30L
private const val DEFAULT_DISK_CACHE_SIZE = 256 * 1024 * 1024L

object OkHttpFactory {
    fun create(context: Context, certificates: Pair<String, Array<String>>? = null) =
        OkHttpClient.Builder()
            .readTimeout(DEFAULT_READ_TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .connectTimeout(DEFAULT_CONNECT_TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .cache(Cache(context.cacheDir, DEFAULT_DISK_CACHE_SIZE))
            .apply {
                if (certificates != null) {
                    this.certificatePinner(
                        CertificatePinner.Builder()
                            .add(certificates.first, *certificates.second)
                            .build()
                    )
                }
            }
}